#!/usr/bin/env bash
today=`date +%Y-%m-%d--%H-%M-%S`
tar -cvzf /mnt/scrapy/snapshots/jstage/jstage-$today.tgz /mnt/scrapy/jobs/jstage*