#!/usr/bin/env bash
pid=`cat jstage.pid`
kill $pid
echo "Waiting for process to stop..."
while ps -p `cat jstage.pid` > /dev/null; do sleep 1; done
echo "Done"