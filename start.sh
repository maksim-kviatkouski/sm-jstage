#!/usr/bin/env bash
today=`date +%Y-%m-%d--%H-%M-%S`
nohup scrapy crawl jstage_article_links_ -a url_list=s3://scraping.items/jstage_journal_links/2015-06-02T12-33-08_upd.jsonlines -a start=0 -a end=100 >> ~/logs/jstage/output_$today.log &
rm -f output.log
ln -s ~/logs/jstage/output_$today.log output.log
echo "$!" > jstage.pid