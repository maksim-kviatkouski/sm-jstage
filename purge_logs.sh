#!/usr/bin/env bash
for f in `find ~/logs/jstage -name *.log -mtime +1`; do aws s3 cp $f s3://scraping.logs/jstage/; rm -f $f; done;