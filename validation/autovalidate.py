import sys
from pymongo import MongoClient

__author__ = 'Maksim_Kviatkouski'
from subprocess import call
from sys import argv, exit

s3url = argv[1] if argv[1] else sys.exit("Please provide S3 URL to a file with items in jsonlines format")
collection_name = s3url.split("/")[-2:-1] + "-" + s3url.split("/")[-1:].replace('.xml', '')
call(['aws', 's3', 'cp', s3url, '.'])
client = MongoClient()
db = client.validation
coll  = db[collection_name]
call('mongoimport', '--db', 'validation', '--collection', collection_name, s3url.split("/")[-1:])


