__author__ = 'Maksim_Kviatkouski'
from scrapy.statscol import StatsCollector
import os
import pickle
from scrapy.xlib.pydispatch import dispatcher
from scrapy import signals
from scrapy import log

class PersistentStatsCollector(StatsCollector):
    def __init__(self, crawler):
        super(PersistentStatsCollector, self).__init__(crawler)
        #need to load stats from jobdir here
        self.jobdir = crawler.settings.get('JOBDIR')
        if self.jobdir and os.path.exists(self.statefn):
            with open(self.statefn, 'rb') as f:
                self._stats = pickle.load(f)
        else:
            self._stats = {}

    def _persist_stats(self, stats, spider):
        if self.jobdir:
            with open(self.statefn, 'wb') as f:
                pickle.dump(stats, f, protocol=2)

    @property
    def statefn(self):
        return os.path.join(self.jobdir, 'crawler.state')


class DumpJournalLinksMiddleware(object):
    def __init__(self, crawler):
        self.jobdir = crawler.settings.get('JOBDIR')
        dispatcher.connect(self.spider_closed, signals.spider_closed)

    def spider_closed(self, spider):
        if self.jobdir and hasattr(spider, 'journal_links'):
            log.msg("Dumping %s journal links upon spider closure" % len(spider.journal_links))
            with open(self.journal_links_fn, 'w') as f:
                for l in spider.journal_links:
                    log.msg("Dumping link: %s" % l, log.DEBUG)
                    f.write(l + '\n')

        if self.jobdir and hasattr(spider, 'article_links'):
            log.msg("Dumping %s article links upon spider closure" % len(spider.article_links))
            with open(self.article_links_fn, 'w') as f:
                for l in spider.article_links:
                    log.msg("Dumping link: %s" % l, log.DEBUG)
                    f.write(l + '\n')

    @property
    def journal_links_fn(self):
        return os.path.join(self.jobdir, 'journal.links.list.txt')

    @property
    def article_links_fn(self):
        return os.path.join(self.jobdir, 'article.links.list.txt')

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler)