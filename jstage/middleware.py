from time import sleep

__author__ = 'Maksim_Kviatkouski'
from scrapy.exceptions import IgnoreRequest
from scrapy import log

class IgnoreRequestsMiddleware(object):
    def __init__(self, settings):
        self.ignore_requests = {r for r in settings.get('IGNORE_REQUESTS', [])}

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler.settings)

    def process_request(self, request, spider):
        if request.url in self.ignore_requests:
            log.msg('Skipped request %s' % request.url, log.DEBUG)
            raise IgnoreRequest()
        else:
            return request

class Set404ToJstageErrorPage(object):
    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler)

    def __init__(self, crawler):
        self.crawler = crawler

    def process_response(self, request, response, spider):
        if response.status == 302 and response.headers.get('Location','').find('/AY03S050') != -1:
            # log.msg("Added 404 to page %s so it's ignored by caching middleware" % response.url)
            # response.status = 404
            self.crawler.engine.pause()
            sleep(10)
            self.crawler.engine.unpause()
            return request
        else:
            return response