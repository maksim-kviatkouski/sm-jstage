from jstage.items import ArticleItem, AuthorItem, ReferenceItem, UrlItem
import jstage

__author__ = 'Maksim_Kviatkouski'
from scrapy import Spider
from scrapy.utils.markup import replace_escape_chars
from datetime import datetime
from scrapy.http import FormRequest, Request
from scrapy import log
import scrapy
import re
from urlparse import urlparse, parse_qs
from jstage import settings
import urllib
import json
import boto

DOMAIN = "https://www.jstage.jst.go.jp"


def process_onclick(val):
    url = val.replace("location.href='/", "")[:-1].replace('/browse/', '/')
    idx = url.find('A_PRedirectJournalInit')
    if idx != -1:
        url = DOMAIN + "/" + url[idx:]
        log.msg("Extracted link %s from onclick attribute: " % url)
        return url
    else:
        log.msg("Ignored link %s from onclick attribute: " % url)
        return None


class JstageSpider(Spider):
    name = 'jstage'
    allowed_domains = ['www.jstage.jst.go.jp']

    def __init__(self, url_list, start, end, *args, **kwargs):
        super(JstageSpider, self).__init__(*args, **kwargs)
        self.url_list = url_list
        self.start = int(start)
        self.end = int(end)
        
    def start_requests(self):
        s3_connection = boto.connect_s3(settings.AWS_ACCESS_KEY_ID, settings.AWS_SECRET_ACCESS_KEY)
        parsed_url = urlparse(self.url_list).path.split("/", 2)
        s3_bucket = s3_connection.get_bucket(parsed_url[1])
        s3_key = s3_bucket.get_key("".join(parsed_url[2:]))
        lines = s3_key.get_contents_as_string().splitlines()
        for line in lines[self.start:self.end]:
            url = json.loads(line)['url']
            yield Request(url, callback=self.extract_article)
        s3_connection.close()

    def extract_article(self, response):
        log.msg("Starting to extract article for %s" % response.url)

        item = ArticleItem()
        item['counter'] = 0
        item['timestamp'] = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        item['spiderVersion'] = jstage.__version__

        item['url'] = response.url
        item['title'] = response.xpath('//h1[@class="mod-page-heading"]/text()[2]').extract()[0].strip()
        volume = response.xpath('//h1[@class="mod-page-heading"]/a[1]/text()').extract()
        item['volume'] = volume[0].replace('Vol. ', '') if volume else ''
        item['year'] = response.xpath('//h1[@class="mod-page-heading"]/text()[3]').extract()[0].strip().replace("(",
                                                                                                                "").replace(
            ")", "")
        number = response.xpath('//h1[@class="mod-page-heading"]/a[2]/text()').extract()
        item['number'] = re.sub(r"No\. ", "", number[0]) if number else ""
        pages = response.xpath('//h1[@class="mod-page-heading"]/text()[6]').extract()
        item['pages'] = pages[0].strip().replace('p. ', '') if pages else ''
        doi = response.xpath('//p[@class="doi"]/text()').extract()
        item['doi'] = doi[0] if doi else ""
        item['article_title'] = response.xpath('//div[@class="mod-article-info"]//h2/text()').extract()[0]

        authorLinks = response.xpath("//div[@class='mod-article-meta']/p[contains(@class, 'author')]/a")
        affiliations = \
        response.xpath("//div[@class='mod-article-meta']/p[contains(@class, 'affiliation')]/text()").extract()[
            0].strip().split("\r\n\r\n")
        affiliations = [a for a in affiliations if a]
        item['author'] = self.parseAuthorsAndAffiliations(authorLinks, affiliations)

        rawReleaseStrs = response.xpath(
            "//div[@class='mod-article-meta']/div[contains(@class, 'date')]/*//text()").extract()
        for i in range(len(rawReleaseStrs)):
            rawReleaseStrs[i] = rawReleaseStrs[i].strip()
        item['releaseInfo'] = replace_escape_chars(" ".join(rawReleaseStrs))

        item['keywords'] = response.xpath("//p[contains(@class, 'keywords')]/a/text()").extract()
        htmlLink = response.xpath("//li[@class='icon-html']/a/@href").extract()
        htmlLink = htmlLink[0] if htmlLink else ""
        # replacing _article with _html in case html link exists instead of extracting that link from page
        # this is not very good but for some reason site gives wrong redirects to scrapy comparing to browser
        item['fullTextHtmlLink'] = item['url'].replace("_article", "_html") if htmlLink != "" else ""
        pdfLink = response.xpath("//li[@class='icon-pdf']/a/@href").extract()
        pdfLink = pdfLink[0] if pdfLink else ""
        item['fullTextPdfLink'] = DOMAIN + pdfLink if pdfLink != "" else ""
        abstracts = response.xpath("//div[@class='mod-section']/p/text()").extract()
        item['abstracts'] = " ".join(abstracts).strip() if abstracts else ""
        item['copyright'] = response.xpath("//div[@class='mod-article-copyright']/text()").extract()[0].strip().replace(
            "\t", "").replace("\r", "").replace("\n", "")
        issns = response.xpath("//div[@class='str-header-banner']//form/div/text()").extract()[0].strip().split("\r\n")
        item['onlineISSN'] = ""
        item['printISSN'] = ""
        # there may be no issn, or just one and I cannot rely on order of ISSNs in markup so
        # need to do this iterative check
        for issn in issns:
            if "ONLINE ISSN:" in issn:
                item['onlineISSN'] = re.sub(r"ONLINE ISSN: ", "", issn)
            elif "PRINT ISSN:" in issn:
                item['printISSN'] = re.sub(r"PRINT ISSN: ", "", issn)
            else:
                pass
        item['article_type'] = "".join(
            response.xpath("//div[@class='mod-page-heading-container']/text()").extract()).strip()

        referencesExtracted = response.xpath("//li/a[contains(.,'References')]/@href").extract()
        referncesUrl = DOMAIN + referencesExtracted[0] if referencesExtracted else ""
        if referncesUrl:
            item['counter'] += 1
            log.msg("Yielding references request for %s" % response.url)
            referencesRequest = scrapy.Request(
                referncesUrl, callback=self.extract_references, priority=5,
                dont_filter=True, meta={'item': item}, errback=self.on_error)
            yield referencesRequest

        # japaneseFieldsUrl = DOMAIN + response.xpath("//div[@class='lang']/a/@href").extract()[0]
        # if japaneseFieldsUrl:
        japaneseFieldsUrl = response.url + "/-char/ja/"
        item['counter'] += 1
        log.msg("Yielding Japanese fields request for %s" % response.url)
        japaneseFieldsRequest = scrapy.Request(
            japaneseFieldsUrl, callback=self.extract_japanese_fields, priority=5,
            dont_filter=True, meta={'item': item}, errback=self.on_error)
        yield japaneseFieldsRequest

        if item['fullTextHtmlLink'] != "":
            item['counter'] += 1
            log.msg("Yielding full text html request for %s" % response.url)
            fullTextHtmlRequest = scrapy.Request(
                item['fullTextHtmlLink'], callback=self.go_to_full_text_html, priority=5,
                meta={'item': item}, dont_filter=True, errback=self.on_error)
            yield fullTextHtmlRequest

        yield item


    def extract_references(self, response):
        log.msg("References extraction is called for %s" % response.url)
        item = response.meta['item']
        refSelectors = response.xpath('//ul[@class="mod-list-citation"]/li')
        references = []
        for refSel in refSelectors:
            reference = ReferenceItem()
            link = refSel.xpath("a/@href").extract()
            reference['link'] = link[0] if link else ""
            referenceTextParts = refSel.xpath(".//text()").extract()
            for idx, part in enumerate(referenceTextParts):
                referenceTextParts[idx] = referenceTextParts[idx].strip()
            reference['text'] = " ".join(referenceTextParts)
            references.append(dict(reference))
        item['references'] = references
        item['counter'] -= 1
        return item


    def extract_japanese_fields(self, response):
        log.msg("Extracting Japanese fields for %s" % response.url)
        item = response.meta['item']
        jTitle = response.xpath("//h1[@class='mod-page-heading']/text()[2]").extract()
        item['jTitle'] = jTitle[0] if jTitle else ""
        jArticleTitleParts = response.xpath("//h2[contains(@class, 'mod-article-heading')]//text()").extract()
        for idx, part in enumerate(jArticleTitleParts):
            jArticleTitleParts[idx] = part.strip()
        item['jArticle_title'] = " ".join(jArticleTitleParts)
        item['jKeywords'] = response.xpath("//p[contains(@class, 'keywords')]/a/text()").extract()
        jCopyright = response.xpath("//div[@class='mod-article-copyright']/text()").extract()
        item['jCopyright'] = replace_escape_chars(jCopyright[0].strip()) if jCopyright else ""

        authorLinks = response.xpath("//div[@class='mod-article-meta']/p[contains(@class, 'author')]/a")
        affiliations = \
        response.xpath("//div[@class='mod-article-meta']/p[contains(@class, 'affiliation')]/text()").extract()[
            0].strip().split("\r\n\r\n")
        affiliations = [a for a in affiliations if a]
        item['jAuthor'] = self.parseAuthorsAndAffiliations(authorLinks, affiliations)

        item['counter'] -= 1
        return item


    def go_to_full_text_html(self, response):
        log.msg("Extracting link to images page from full text html for %s" % response.url)
        item = response.meta['item']
        figuresUrl = response.xpath("//li[@class='icon-key']/a[contains(.,'Figures')]/@href").extract()
        if len(figuresUrl) > 0:
            # we do not increasing counter here since this request just continues existing thread
            # we could decrease counter (since we're returning from callback) and increase it since we yield another request
            # which equals to leaving counter as is
            log.msg("Yielding images request for %s" % response.url)
            figuresRequest = scrapy.Request(
                DOMAIN + figuresUrl[0], callback=self.parse_images, priority=5,
                dont_filter=True, meta={'item': item}, errback=self.on_error)
            return figuresRequest
        else:
            item['counter'] -= 1
            return item


    def parse_images(self, response):
        log.msg("Extracting links to images for %s" % response.url)
        item = response.meta['item']
        imgLinks = response.xpath("//div[@class='mod-layout-media']//a/@href").extract()
        item['imageUrls'] = []
        for imgLink in imgLinks:
            if imgLink:
                item['imageUrls'].append(DOMAIN + imgLink)

        item['counter'] -= 1
        return item


    def on_error(self, response):
        log.msg("%s resulted in %s" % (response.url, response.status), level=log.ERROR)


    def parseAuthorsAndAffiliations(self, author_links, affiliations):
        result = []

        if not author_links and not affiliations:
            return result
        authToIndexesDict = {}
        for aLink in author_links:
            affParts = aLink.xpath(".//text()").extract()
            authName = affParts[0] if len(affParts) > 0 else ""
            authAffs = affParts[1].split(")") if len(affParts) > 1 else []
            cleanAuthAffs = []
            for aff in authAffs:
                if aff.strip():
                    cleanAuthAffs.append(aff.strip())
            authToIndexesDict[authName] = cleanAuthAffs;
        indexToAffiliationDict = {}
        for aff in affiliations:
            affParts = aff.split(")")
            if (affParts):
                affIndex = affParts[0]
                affName = affParts[1]
                indexToAffiliationDict[affIndex] = affName.strip()
        for author, index in authToIndexesDict.iteritems():
            authorItem = AuthorItem()
            authorItem['authorName'] = author
            authorItem['authorAddress'] = []
            for affIndex in authToIndexesDict[author]:
                authorItem['authorAddress'].append(indexToAffiliationDict.get(affIndex,''))
            result.append(authorItem)
        return result


class ListingSpider(Spider):
    name = 'jstage_journal_links'
    # allowed_domains = ['www.jstage.jst.go.jp']
    start_urls = ['https://www.jstage.jst.go.jp/AF03S010Init']

    def __init__(self, *a, **kw):
        super(ListingSpider, self).__init__(*a, **kw)
        self.journal_links = {l for l in self.state.get('journal_links', [])} if hasattr(self, 'state') else set()

    def start_requests(self):
        req = FormRequest('https://www.jstage.jst.go.jp/AF03S010HyjKnsu',
                           formdata={'__multiselect_srySybt': '',
                                     '__multiselect_srySybt': '',
                                     '__checkbox_newSry': 'true',
                                     '__multiselect_kijiLang': '',
                                     '__multiselect_kijiLang': '',
                                     '__multiselect_kijiLang': '',
                                     'categoryCount': '24',
                                     'hyjKnsuAraJyb': '2000'}
                           )
        # req.meta['_dont_cache'] = True
        yield req

    #parsing article listing page here
    def parse(self, response):
        log.msg("Parsing %s" % response.url, log.DEBUG)
        journal_links = response.xpath('//table[@id="journal_table"]//tr//a[contains(@href, "A_PRedirectJournalInit")]/@href').extract()
        log.msg("Got %r links to extract" % len(journal_links))
        for h in journal_links:
            log.msg("Yielding request to a link %s" % h, log.DEBUG)
            journal_code = parse_qs(urlparse(h.encode('ascii', 'ignore')).query)['sryCd'][0]
            i = UrlItem()
            i['url'] = '%s/browse/%s' % (DOMAIN, journal_code)
            yield i

class ArticleListingSpider(Spider):
    name = 'jstage_article_links_'
    allowed_domains = ['www.jstage.jst.go.jp']

    def __init__(self, url_list = None, start = None, end = None, parse_listing_only = False, *a, **kw):
        self.start = int(start) if start else 0
        self.end = int(end)
        self.url_list = url_list
        self.parse_listing_only = bool(parse_listing_only)

    def start_requests(self):
        s3_connection = boto.connect_s3(settings.AWS_ACCESS_KEY_ID, settings.AWS_SECRET_ACCESS_KEY)
        parsed_url = urlparse(self.url_list)
        s3_bucket = s3_connection.get_bucket(parsed_url.netloc)
        s3_key = s3_bucket.get_key(parsed_url.path)
        lines = s3_key.get_contents_as_string().splitlines()
        callback = self.parse if not self.parse_listing_only else self.parse_listing
        for line in lines[self.start:self.end]:
            url = json.loads(line)['url']
            yield Request(url, callback=callback)

    def parse(self, response):
        #parse volumes here
        for l in response.xpath('//h4[contains(./font,"Volume And Issue List")]/following-sibling::div//a/@onclick').extract():
            redirectUrl = l.replace("location.href='",'')[:-1].encode('ascii', 'ignore')
            params = parse_qs(urlparse(redirectUrl).query)
            yield Request("%s/browse/%s/%s/%s/_contents" % (DOMAIN, params['sryCd'][0], params['noVol'][0], params['noIssue'][0]),
                          callback=self.parse_listing, headers={'Referer': response.url})

    def parse_listing(self, response):
        #parse links and yield requests to other volumes with parse_article_list callback
        articleLinkSelectors = response.xpath('//div[@class="str-main"]/div[@class="mod-item"]//h3[@class="mod-item-heading"]')
        japaneseOnlyArticleNumbers = set()
        for idx, a in enumerate(articleLinkSelectors):
            if a.xpath('./a/@href'):
                qParams = parse_qs(urlparse(a.xpath('./a/@href').extract()[0].encode('ascii', 'ignore')).query)
                resultingUrl = '%s/article/%s/%s/%s/%s/_article' % (DOMAIN, qParams['sryCd'][0], qParams['noVol'][0], qParams['noIssue'][0], qParams['kijiCd'][0])
                log.msg('Restored url from params: %s' % resultingUrl)
                i = UrlItem()
                i['url'] = resultingUrl
                yield i
            else:
                japaneseOnlyArticleNumbers.add(idx)
        if japaneseOnlyArticleNumbers:
            o = urlparse(response.url)
            yield Request('%s://%s%s/-char/ja/?%s' % (o.scheme, o.netloc, o.path, o.query),
                          meta={'japanese_only': japaneseOnlyArticleNumbers}, callback=self.parse_japanese_only,
                          headers={'Referer': response.url})
        nextPageSel = response.xpath('//div[@id="index"]//li[@class="jump"]//a[text()=">"]/@href')
        if nextPageSel:
            log.msg('Request for next page: %s' % nextPageSel.extract()[0])
            yield Request(DOMAIN + nextPageSel.extract()[0], callback=self.parse_listing, headers={'Referer': response.url})

    def parse_japanese_only(self, response):
        articleLinkSelectors = response.xpath('//div[@class="str-main"]/div[@class="mod-item"]//h3[@class="mod-item-heading"]')
        japaneseOnlyArticleNumbers = response.meta.get('japanese_only', set())
        for idx, a in enumerate(articleLinkSelectors):
            if idx in japaneseOnlyArticleNumbers:
                qParams = parse_qs(urlparse(a.xpath('./a/@href').extract()[0].encode('ascii', 'ignore')).query)
                resultingUrl = '%s/article/%s/%s/%s/%s/_article' % (DOMAIN, qParams['sryCd'][0], qParams['noVol'][0], qParams['noIssue'][0], qParams['kijiCd'][0])
                log.msg('Restored url from params: %s' % resultingUrl)
                i = UrlItem()
                i['url'] = resultingUrl
                yield i
