# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy.xlib.pydispatch import dispatcher
from scrapy import signals
from jstage.exporters import NlmXmlItemExporter
from scrapy.exceptions import DropItem
import datetime
from scrapy import log

class JstageDedpulicationPipeline(object):
    def __init__(self):
        dispatcher.connect(self.spider_opened, signals.spider_opened)
        dispatcher.connect(self.spider_closed, signals.spider_closed)

    def process_item(self, item, spider):
        if hasattr(item, 'counter') and item['counter'] != 0:
            log.msg('It\'s not a complete item yet. Dropping it: %s' % item.get('url',''))
            raise DropItem('It\'s not a complete item yet. Dropping it.')
        else:
            return item

    def spider_opened(self, spider):
        pass

    def spider_closed(self, spider):
        pass

class JstagePipeline(object):
    def __init__(self):
        dispatcher.connect(self.spider_opened, signals.spider_opened)
        dispatcher.connect(self.spider_closed, signals.spider_closed)
        self.files = {}

    def spider_opened(self, spider):
        file = open('%s_articles_'+datetime.now().strftime("%Y_%m_%d__%H_%M_%S")+'.xml' % spider.name, 'w+b')
        self.files[spider] = file
        self.exporter = NlmXmlItemExporter(file)
        self.exporter.start_exporting()

    def spider_closed(self, spider):
        self.exporter.finish_exporting()
        file = self.files.pop(spider)
        file.close()

    def process_item(self, item, spider):
        self.exporter.export_item(item)
        return item
