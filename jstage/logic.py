from history.logic import RetrieveBase, StoreBase

__author__ = 'Maksim_Kviatkouski'

class RetrieveRespectDontCache(RetrieveBase):
    def retrieve_if(self, spider, request):
        return not request.meta.get('_dont_cache', False)