# -*- coding: utf-8 -*-

BOT_NAME = 'jstage'

SPIDER_MODULES = ['jstage.spiders']
NEWSPIDER_MODULE = 'jstage.spiders'

ITEM_PIPELINES = {
    'jstage.pipelines.JstageDedpulicationPipeline': 100
}

DOWNLOADER_MIDDLEWARES = {
    'history.middleware.HistoryMiddleware': 901,
    'jstage.middleware.Set404ToJstageErrorPage': 905
}

EPOCH = True
HISTORY = {
        'STORE_IF'   : 'history.logic.StoreAlways',
        'RETRIEVE_IF': 'jstage.logic.RetrieveRespectDontCache',
        'BACKEND'    : 'history.storage.S3CacheStorage',
        'S3_ACCESS_KEY': 'AKIAJ5OEW7R44CI67TNQ',
        'S3_SECRET_KEY': '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',
        'S3_BUCKET'    : 'jstage.cache',
        'USE_PROXY'  : False,
}

FEED_URI = 's3://scraping.items/%(name)s/%(time)s.jsonlines'
FEED_FORMAT = 'jsonlines'
AWS_ACCESS_KEY_ID = 'AKIAJ5OEW7R44CI67TNQ'
AWS_SECRET_ACCESS_KEY = '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'

CONCURRENT_REQUESTS = 100
CONCURRENT_REQUESTS_PER_DOMAIN = 100
CONCURRENT_ITEMS = 100

RETRY_TIMES = 20

IGNORE_REQUESTS = [
    'https://www.jstage.jst.go.jp/pub/html/AY03S050_en.html'
]

HTTPCACHE_IGNORE_HTTP_CODES = [404, 500, 503]