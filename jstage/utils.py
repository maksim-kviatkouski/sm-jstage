__author__ = 'Maksim_Kviatkouski'
from history.storage import S3CacheStorage
from scrapy import Request
from scrapy.conf import settings
import boto

def getS3KeyForUrl(url, spider, cache):
    return cache._get_s3_key(cache._get_key(spider, Request(url)), True)

def deleteS3EntriesFromFile(file, spider):
    s3cache = S3CacheStorage()
    s3cache.open_spider(spider)
    s3_connection = boto.connect_s3(settings.get('HISTORY').get('S3_ACCESS_KEY'), settings.get('HISTORY').get('S3_SECRET_KEY'))
    s3_bucket = s3_connection.get_bucket(settings.get('HISTORY').get('S3_BUCKET'), validate=False)
    f = open(file, 'r')
    for line in f:
        key = getS3KeyForUrl(line.strip(), spider, s3cache)
        if key:
            s3_bucket.delete_key(key)
            print "Deleted %s => %r" % (line, key)
    f.close()
    s3_connection.close()