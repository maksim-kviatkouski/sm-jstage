__author__ = 'Maksim_Kviatkouski'
from scrapy.contrib.exporter import XmlItemExporter
import itertools

class NlmXmlItemExporter(XmlItemExporter):

    def write_tag(self, name, value = '', attr = {}):
        self.xg.startElement(name, attr)
        self.xg.characters(value)
        self.xg.endElement(name)

    def export_item(self, item):
        if item['counter'] != 0:
            print "Not serializing this item since not all requests for it were completed"
            return None
        self.xg.startElement("article",
                             {'id': item['url'], 'spider-name': 'jstage',
                              'spider-version': item['spiderVersion'],
                              'crawl-date': item['timestamp']})
        self.xg.startElement("front", {})
        self.xg.startElement("article-meta", {})
        self.write_tag("abstract", item.get('abstracts',''))
        self.write_tag("article-id", item['url'], {'pub-id-type': 'jstage-url'})
        self.write_tag("article-id", item['doi'], {'pub-id-type': 'doi'})
        self.xg.startElement("article-group",{})
        self.xg.startElement("subj-group",{})
        self.write_tag("subj", item['article_type'])
        self.xg.endElement("subj-group")
        self.xg.endElement("article-group")
        self.xg.startElement("title-group", {})
        self.write_tag("article-title", item['article_title'], {'xml:lang': 'en'})
        if (item.get('jArticle_title',[])):
            self.write_tag("article-title", item['jArticle_title'], {'xml:lang': 'jp'})
        self.xg.endElement("title-group")
        self.write_tag("volume", item['volume'])
        self.write_tag("pub-date", item['year'])
        self.write_tag("issue-part", item['number'])
        self.write_tag("page-range", item['pages'])
        self.xg.startElement("contrib-group", {})
        for authEn, authJa in itertools.izip_longest(item.get('author',[]), item.get('jAuthor',[])):
            self.xg.startElement("contrib", {'contrib-type': 'author'})
            if authEn:
                # temporary untill first/last names are parsed
                self.write_tag("name", authEn['authorName'], {'name-style': 'western'})
                for addr in authEn['authorAddress']:
                    self.write_tag("aff", addr)
            if authJa:
                self.write_tag("string-name", authJa['authorName'], {'name-style': 'eastern', 'xml:lang': 'jp'})
                for addr in authJa['authorAddress']:
                    self.write_tag("aff", addr, {'xml:lang': 'jp'})
            self.xg.endElement("contrib")
        self.xg.endElement("contrib-group")
        self.xg.startElement("kwd-group", {'xml:lang': 'en'})
        for kwd in item['keywords']:
            self.write_tag("kwd", kwd)
        self.xg.endElement("kwd-group")
        self.xg.startElement("kwd-group", {'xml:lang': 'jp'})
        for jKwd in item['jKeywords']:
            self.write_tag("kwd", jKwd)
        self.xg.endElement("kwd-group")
        if item['fullTextHtmlLink']:
            self.write_tag("related-object", "", {'source-id': item['fullTextHtmlLink'], 'source-id-type': 'url', 'content-type': 'html'})
        if item['fullTextPdfLink']:
            self.write_tag("related-object", "", {'source-id': item['fullTextPdfLink'], 'source-id-type': 'url', 'content-type': 'pdf'})
        for imgUrl in item.get('imageUrls', []):
            self.write_tag("related-object", "", {'source-id': imgUrl, 'source-id-type': 'url', 'content-type': 'image'})

        self.xg.startElement("journal-meta", {})
        self.write_tag("issn", item.get('printISSN',''), {'publication-format': 'print'})
        self.write_tag("issn", item.get('onlineISSN',''), {'publication-format': 'electronic'})
        self.xg.startElement("journal-title-group", {})
        self.write_tag("journal-title", item.get('title',''), {'xml:lang': 'en'})
        self.write_tag("journal-title", item.get('jTitle',''), {'xml:lang': 'jp'})
        self.xg.endElement("journal-title-group")
        self.xg.endElement("journal-meta")

        self.xg.endElement("article-meta")
        self.xg.endElement("front")

        self.xg.startElement("back", {})
        self.xg.startElement("ref-list", {})
        for ref in item.get('references', []):
            self.xg.startElement("ref", {})
            if ref.get('link',''):
                self.write_tag("ext-link", "", {'xlink:href': ref['link'], 'xlink:title': ref.get('text','')})
            else:
                self.write_tag("article_title", ref.get('text',''))
            self.xg.endElement("ref")
        self.xg.endElement("ref-list")
        self.xg.endElement("back")

        self.xg.endElement("article")

    def start_exporting(self):
        self.xg.startDocument()
        self.xg.startElement("articles", {})

    def finish_exporting(self):
        self.xg.endElement("articles")
        self.xg.endDocument()