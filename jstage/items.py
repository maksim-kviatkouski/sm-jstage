# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class ArticleItem(scrapy.Item):
    counter = scrapy.Field()
    timestamp = scrapy.Field()
    spiderVersion = scrapy.Field()

    url = scrapy.Field()
    title = scrapy.Field()
    jTitle = scrapy.Field()
    volume = scrapy.Field()
    year = scrapy.Field()
    number = scrapy.Field()
    pages = scrapy.Field()
    doi = scrapy.Field()
    article_title = scrapy.Field()
    jArticle_title = scrapy.Field()
    author = scrapy.Field()
    jAuthor = scrapy.Field()
    releaseInfo = scrapy.Field()
    keywords = scrapy.Field()
    jKeywords = scrapy.Field()
    fullTextHtmlLink = scrapy.Field()
    fullTextPdfLink = scrapy.Field()
    abstracts = scrapy.Field()
    references = scrapy.Field()
    copyright = scrapy.Field()
    jCopyright = scrapy.Field()
    onlineISSN = scrapy.Field()
    printISSN = scrapy.Field()
    article_type = scrapy.Field()
    imageUrls = scrapy.Field()

class AuthorItem(scrapy.Item):
    authorName = scrapy.Field()
    authorAddress = scrapy.Field()

class ReferenceItem(scrapy.Item):
    link = scrapy.Field()
    text = scrapy.Field()

class UrlItem(scrapy.Item):
    url = scrapy.Field()