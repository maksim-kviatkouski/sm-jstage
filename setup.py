# Automatically created by: shub deploy

from setuptools import setup, find_packages
import jstage

setup(
    name         = 'jstage',
    version      = jstage.__version__,
    packages     = find_packages(),
    entry_points = {'scrapy': ['settings = jstage.settings']},
)
