#!/usr/bin/env bash
crontab -l > cron.tmp
echo "35 * * * * bash ~/spiders/jstage/stop.sh & ~/spiders/jstage/job_snapshots.sh & ~/spiders/jstage/start.sh" >> cron.tmp
crontab cron.tmp
rm cron.tmp